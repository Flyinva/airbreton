
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `airbreizh_atmo`;
CREATE TABLE `airbreizh_atmo` (
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `number` tinyint(1) NOT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `airbreton_luftdaten`;
CREATE TABLE `airbreton_luftdaten` (
  `id` int(11) NOT NULL,
  `name` varchar(15) NOT NULL,
  `zone` varchar(31) NOT NULL DEFAULT 'Rennes Métropole',
  `displayed` enum('oui','none') NOT NULL DEFAULT 'oui',
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `indoor` enum('oui','non') NOT NULL DEFAULT 'non',
  `updated` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

