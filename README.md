## Contexte

L'association [AirBreizh](http://www.airbreizh.asso.fr/) publie ses [données de qualité de l'air](http://data.airbreizh.asso.fr/contenu/services_didon.html?service=mes).

[Ambassad'Air](http://www.wiki-rennes.fr/Ambassad%27Air) est une opération portée par la Ville de Rennes et la [Maison de la consommation et de l'environnement](http://www.wiki-rennes.fr/Maison_de_la_consommation_et_de_l%27environnement) qui vise à déployer des capteurs citoyens. Les données des [capteurs Luftdaten](https://luftdaten.info/fr/accueil/) sont accessibles librement.

Ce projet permet de stocker les données AirBreizh et Luftdaten pour les visualiser.

## Scripts

Des scripts BASH téléchargent les données fournies par [AirBreizh](http://data.airbreizh.asso.fr/contenu/services_didon.html?service=mes) et [Luftdaten](https://github.com/opendata-stuttgart/meta/wiki/APIs) au format JSON. Les données sont « digérées » et mises en forme par l'excellent [jq](https://stedolan.github.io/jq/). Elles sont ensuite envoyées à un serveur Graphite local (les envoyer à un serveur distant est possible).

Un fichier de configuration devra être créé dans `$HOME/.config/airbreton/config`. Il devra contenir le mot de passe de la base MySQL et les identifiants des capteurs à exclure de la collecte de données Luftdaten. Le répertoire `$HOME/.conf/airbretons/locations/` contiendra des fichiers JSON avec les coordonnées de capteurs à afficher sur la carte Grafana.


## graphite

Les paramètres de stockage carbon sont fournis dans le fichier `storage-schemas.conf`.


## Grafana

Les données stockées dans Graphite peuvent être exploitées dans Grafana :  un [exemple pour peut-être consulté](https://grafana.kabano.net/d/000000021/synthese). Les tableaux de bord sont stockés dans le répertoire `grafana_dashboards`.

## Base SQL

Une base de données MariaDB est crée. Pour le moment, elle ne contiendra qu'une table avec les indices ATMO. Le schéma de la base est fourni.

